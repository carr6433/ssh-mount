# README #

Ever wanted a better way to manage remote file systems?

clone and run `sudo setup.sh` to get started!


Then, for any host you have set up publickey access to:

    ssh-mount <remote hostname>

When you're done:

    ssh-mount -u <remote hostname>




License: MIT


### What is this repository for? ###

A simple set of scripts to manage sshfs mounted hosts

### How do I get set up? ###

Install is simple but untested.  Run 

    sudo setup.sh

or

    sudo setup.sh -i <install directory> -d <mount directory>

to install the scripts to `/opt/ssh-mount/bin` or `<install directory>/bin` and mount file systems in `/mnt/ssh-mount/<remote hostname>` or `<mount directory>/<remote hostname>`

### How do I use this? ###

To mount a hosts file system run

    ssh-mount <remote hostname>

or, if you need a different user for ssh

    ssh-mount <remote hostname> --user <username>

Mounted file systems are placed in `/mnt/ssh-mount/<remote hostname>` and a link to to that directory is placed at `~/<remote hostname>`


NOTE: This only works if you have publickey access to the remote host WITHOUT a passphrase on your private key.  I'm working on password/passphrase authentication.


### Contribution guidelines ###

Use/modify at your own risks!

Feel free to branch and use how you will.  Contact me if you want to talk about a merge to master.


### Who do I talk to? ###

[Nick Carroll](mailto:nick@carroll-industries.com)

### License: MIT ###
Copyright (C) 2016 Nicholas Carroll, Carroll Industries


Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL NICHOLAS CARROLL OR CARROLL INDUSTRIES BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of Nicholas Carroll or Carroll Industries shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization from Nicholas Carroll and Carroll Industries.