#! /bin/bash

if [[ -z "$SSH_MOUNT_DIR" ]] ; then
    source /etc/profile.d/ssh-mount.sh &>/dev/null
    if [[ -z "$SSH_MOUNT_DIR" ]] ; then
	echo "SSH_MOUNT_DIR environmental variable not set!"
	echo "run 'source /etc/profile.d/ssh-mount.sh' and try again"
	echo "OR"
	echo "logout and login to refresh the bash profile"
    exit
    fi
fi

USAGE="Usage: ssh-mount [OPTIONS]... HOST... [-u, --umount HOST...]"

# perform printf with extra / removed from paths
print() {
    printf "$1" | sed s#//*#/#g && return 0
    return 1
}

help() {
    print "USAGE: $USAGE
ssh-mount is a wrapper for mounting and unmounting filesystems for remote
host(s).
Remote host file systems are mounted to $SSH_MOUNT_DIR/$USER or $SSH_MOUNT_DIR/common
Links to each mounted file system are included at ~/<HOSTNAME>

File systems mounted to $SSH_MOUNT_DIR/common will be avaiable to all members of 
the ssh-mount group, file systems mounted in $SSH_MOUNT_DIR/$USER will only be 
available to $USER

Mandatory arguments to long options are mandatory for short options too.
  -U, --user USER            USER is the ssh user to connect to remote
                                machines, defaults to $USER
  -c, --common               Mount or unmount hosts into the
                               $SSH_MOUNT_DIR/common directory
  -u, --umount HOST...      Unmount a single host filesystem or a list of host
                               filesystems,  everything after the -u option
                               will be unmounted

exit status:
0  if OK,
1  if minor problems occur, but the operation completed successfully
2 if the operation could not complete\n"
    return 0
}

SUB_DIR=$USER
LOGIN=$USER
MOUNT_HOSTS=()
UMOUNT_HOSTS=()
HOST_PTR=MOUNT_HOSTS

# handle args
if [[ -z "$1" ]] || [[ $#<1 ]] ; then
    print "ssh-mount summary for $USER:"
    USER_COUNT=0
    USER_MOUNTS=""
    for h in $(ls -d $SSH_MOUNT_DIR/$USER/*/ 2>/dev/null); do
	if [[ -z "$h" ]] ; then
	    continue
	fi
	USER_MOUNTS="$USER_MOUNTS\t$h\n"
	((USER_COUNT++))
    done
    COMMON_COUNT=0
    COMMON_MOUNTS=""
    for h in $(ls -d $SSH_MOUNT_DIR/common/*/ 2>/dev/null); do
	if [[ -z "$h" ]] ; then
	    continue
	fi
	COMMON_MOUNTS="$COMMON_MOUNTS\t$h\n"
	((COMMON_COUNT++))
    done
    if  [[ "$USER_COUNT" -eq 0 ]] && [[ "$COMMON_COUNT" -eq 0 ]] ; then
	printf "\n\tNo filesystems mounted\n"
    else
	printf "\n--Mounted Host Filesystems--\n"
	
	if [[ "$USER_COUNT" -gt 0 ]] ; then
	    print "$SSH_MOUNT_DIR/$USER\n"
	    print "$USER_MOUNTS"
	fi
	if [[ "$COMMON_COUNT" -gt 0 ]] ; then
	    print "$SSH_MOUNT_DIR/common:\n"
	    print "$COMMON_MOUNTS"
	fi
    fi
    exit 0
fi

while [ "$1" ] ; do
    [[ "$1" == "--help" ]] && help && exit 0
    [[ "$1" == "-h" ]] && echo "$USAGE" && exit 0
    [[ "$1" == "-u" ]] && HOST_PTR=UMOUNT_HOSTS && shift && continue
    [[ "$1" == "--umount" ]] && HOST_PTR=UMOUNT_HOSTS && shift && continue
    [[ "$1" == "--common" ]] && SUB_DIR="common" && shift && continue
    [[ "$1" == "-c" ]] && SUB_DIR="common" && shift && continue
    [[ "$1" == "--user" ]] && LOGIN=$2 && shift && shift && continue
    [[ "$1" == "-U" ]] && LOGIN=$2 && shift && shift && continue
    [[ "$1" == -* ]] && echo "Bad option, '$1'" && echo "$USAGE" &&
	echo "run 'ssh-mount --help' for options list" && exit 2
    eval "$HOST_PTR+=($1)"
    shift
done

EXIT_CODE=0

PERMS=700
if [[ $SUB_DIR == 'common' ]] ; then
    PERMS=770
fi

if [[ $MOUNT_HOSTS# > 0 && ! -d "$SSH_MOUNT_DIR/$SUB_DIR" ]] ; then
    if ! ( mkdir $SSH_MOUNT_DIR/$SUB_DIR &>/dev/null ) ; then
	print "unable to create $SSH_MOUNT_DIR/$SUB_DIR\n"
	exit 2
    fi
    if ! ( chgrp ssh-mount $SSH_MOUNT_DIR/$SUB_DIR &>/dev/null ) ; then
	print "unable to set $SSH_MOUNT_DIR/$SUB_DIR group to ssh-mount\n"
	EXIT_CODE=1
    fi
    if ! ( chmod $PERMS $SSH_MOUNT_DIR/$SUB_DIR &>/dev/null ) ; then
	print "unable to set $SSH_MOUNT_DIR/$SUB_DIR permissions to $PERMS\n"
	EXIT_CODE=1
    fi
fi

# remove duplicate hosts in MOUNT and UMOUNT
MOUNT_HOSTS=($(printf "%s\n" "${MOUNT_HOSTS[@]}" | sort -u))
UMOUNT_HOSTS=($(printf "%s\n" "${UMOUNT_HOSTS[@]}" | sort -u))


for a in "${MOUNT_HOSTS[@]}"; do
    if [[ -d "$SSH_MOUNT_DIR/$SUB_DIR/$a" ]] ; then
	print "$a appears to be mounted at $SSH_MOUNT_DIR/$SUB_DIR/$a, "
	print "try to run 'ssh-mount -u $a'"
	if [[ $SUB_DIR == "common" ]] ; then
	    printf " --common"
	fi
	print " first\n"
	EXIT_CODE=1
	continue
    fi
    if ! ( mkdir $SSH_MOUNT_DIR/$SUB_DIR/$a &>/dev/null ) ; then
	print "Unable to create $SSH_MOUNT_DIR/$SUB_DIR/$a\n"
	EXIT_CODE=1
	continue
    fi
    if ( chgrp ssh-mount $SSH_MOUNT_DIR/$SUB_DIR/$a &>/dev/null ) ; then	
	print "Mounting $LOGIN@$a:/ at $SSH_MOUNT_DIR/$SUB_DIR/$a\n"
    else
	print "Unable to set $SSH_MOUNT_DIR/$SUB_DIR/$a group to ssh-mount\n"
	EXIT_CODE=1
    fi
    if ! ( chmod $PERMS $SSH_MOUNT_DIR/$SUB_DIR/$a &>/dev/null ) ; then
	print "Unable to set $SSH_MOUNT_DIR/$SUB_DIR/$a permissions to $PERMS\n"
	EXIT_CODE=1
    fi

    if ( sshfs $LOGIN@$a:/ $SSH_MOUNT_DIR/$SUB_DIR/$a ) ; then
	print "$LOGIN@$a:/ mounted at $SSH_MOUNT_DIR/$SUB_DIR/$a\n"
    else
	print "Unable to mount $LOGIN@$a:/ at $SSH_MOUNT_DIR/$SUB_DIR/$a\n" 
	rmdir $SSH_MOUNT_DIR/$SUB_DIR/$a &>/dev/null 
	rmdir $SSH_MOUNT_DIR/$SUB_DIR &>/dev/null
	EXIT_CODE=1
	continue
    fi
    
    if [[ ! -d "$SSH_MOUNT_DIR/$SUB_DIR/$a" ]] ; then
	continue
    fi
    
    LINK=~/$a
    if [[ $SUB_DIR == 'common' ]] ; then
	LINK=~/$a-common
    fi
    
    if ( ln -s $(print $SSH_MOUNT_DIR/$SUB_DIR/$a) $LINK &>/dev/null ) ; then
	print "link $LINK to $SSH_MOUNT_DIR/$SUB_DIR/$a was created\n"
    else
	print "WARNING: Could not link $LINK to $SSH_MOUNT_DIR/$SUB_DIR/$a\n"
	EXIT_CODE=1
    fi
done


for a in "${UMOUNT_HOSTS[@]}"; do
    if [[ ! -d "$SSH_MOUNT_DIR/$SUB_DIR/$a" ]] ; then
	print "$a does not appear to be mounted at $SSH_MOUNT_DIR/$SUB_DIR/$a, did you mean "
	if [[ $SUB_DIR == "common" ]] ; then
	    print "$SSH_MOUNT_DIR/$USER/$a?\n"
	    echo "i.e. 'ssh-mount -u $a'"
	else
	    print "$SSH_MOUNT_DIR/common/$a?\n"
	    echo "i.e. 'ssh-mount -u $a --common'"
	fi
	EXIT_CODE=1
	continue
    else
	fusermount -u $SSH_MOUNT_DIR/$SUB_DIR/$a &>/dev/null	
	if ( rmdir $SSH_MOUNT_DIR/$SUB_DIR/$a &>/dev/null ) ; then
	    print "Unmounted $SSH_MOUNT_DIR/$SUB_DIR/$a\n"
	else
	    print "Failed to unmount $SSH_MOUNT_DIR/$SUB_DIR/$a\n"
	    EXIT_CODE=1
	fi
	
	LINK=~/$a
	if [[ $SUB_DIR == 'common' ]] ; then
	    LINK=~/$a-common
	fi
	if [[ -h $LINK ]] ; then
	    rm $LINK &>/dev/null
	    echo "Removed symlink at $LINK"
	else
	    echo "WARNING: Could not remove symlink at $LINK"
	    EXIT_CODE=1
	fi
    fi
done
if [[ -d "$SSH_MOUNT_DIR/$SUB_DIR" && ! "$(ls -A $SSH_MOUNT_DIR/$SUB_DIR)" ]];
then
    if ! ( rmdir $SSH_MOUNT_DIR/$SUB_DIR 2>/dev/null ) ; then
	print "Could not remove empty directory $SSH_MOUNT_DIR/$SUB_DIR\n"
	EXIT_CODE=1
    fi
fi

exit $EXIT_CODE
