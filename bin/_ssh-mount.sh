_ssh_mount()
{
    local cur prev opts u_flag common_flag
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    u_flag=false
    common_flag=false
    for o in "${COMP_WORDS[@]}" ; do
	[[ $o == "-u" ]] && u_flag=true
	[[ $o == "--umount" ]] && u_flag=true
	[[ $o == "-c" ]] && common_flag=true
	[[ $o == "--common" ]] && common_flag=true
    done

    if [[ ${cur} == --* ]]; then
	opts="--common --umount --user --help"
	COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    else
	if $u_flag && $common_flag ; then
	    opts=$(ls -D $SSH_MOUNT_DIR/common 2>/dev/null)
	elif $u_flag ; then
	    opts=$(ls -D $SSH_MOUNT_DIR/$USER 2>/dev/null)
	else
	    opts=$(awk '{
                          if (length($1)==0) {
                            next
                          } else if ($1=="#") {
                            next
                          } else {
                            print $2
                          }
                        }' /etc/hosts)
	fi

	COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        return 0
    fi
}
complete -F _ssh_mount ssh-mount
