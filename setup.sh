#! /bin/bash

if [ "$EUID" -ne 0 ]
    then echo "Please run as root"
    exit
fi

SSH_MOUNT_DIR="/mnt/ssh-mount"
INSTALL_DIR="/opt/ssh-mount"
RM_FLAG=false
EXIT_CODE=0

USAGE="Usage: setup.sh [-d MOUNT_DIR] [-i INSTALL_DIR]"

# handle args
while [ "$1" ]; do
    [[ "$1" == "-h" ]] && echo "$USAGE" && exit 0
    [[ "$1" == "-d" ]] && SSH_MOUNT_DIR=${2%/} && shift && shift && continue
    [[ "$1" == "-i" ]] && INSTALL_DIR=${2%/} && shift && shift && continue
    [[ "$1" == "--rm" ]] && RM_FLAG=true && shift && continue
    [[ "$1" == -* ]] && echo "Invalid switch, '$1'" && echo "$USAGE" && exit 1
done
[[ -z $SSH_MOUNT_DIR || "$SSH_MOUNT_DIR" == -* ]] &&
    echo "-d switch failed '-d $SSH_MOUNT_DIR'" && echo "$USAGE" && exit 1
[[ -z $INSTALL_DIR || "$INSTALL_DIR" == -* ]] &&
    echo "-i switch failed '-i $INSTALL_DIR'" && echo "$USAGE" && exit 1

# perform uninstall and exit if required ###############################
# remove everything that setup installs.  If you didn't use the
# defaults, you will have to pass in the same -d and -i options
if $RM_FLAG ; then
    echo "!!WARNING!!"
    echo "This COULD remove all files in $SSH_MOUNT_DIR !"
    echo "This WILL remove all files in $INSTALL_DIR !"
    echo "This WILL remove the sshfs package!"
    echo -n "Are you sure?(y/n)"
    read -n 1 proceed
    echo ""
    [[ $proceed == "y" ]] || exit 1

    #remove the mount directory
    if [[ -d "$SSH_MOUNT_DIR" ]] ; then
	( rmdir $SSH_MOUNT_DIR && echo "$SSH_MOUNT_DIR removed" ) ||
	    ( echo "cannot remove $SSH_MOUNT_DIR, are filesystems still mounted?" &&
		    exit 2)
    else
	echo "$SSH_MOUNT_DIR is not a directory"
    fi

    #remove the install directory
    if [[ -d "$INSTALL_DIR" ]] ; then
	( rm -rf $INSTALL_DIR && echo "$INSTALL_DIR removed" ) ||
	    ( echo "cannot remove $INSTALL_DIR" && EXIT_CODE=1 )
    else
	echo "$INSTALL_DIR is not a directory"
    fi
    
    # remove the sshfs package
    ( apt-get autoremove sshfs &&
	    echo "sshfs package removed" ) ||
	( "could not remove sshfs package" && EXIT_CODE=1 )

    # remove the environmental variables
    ( rm /etc/profile.d/ssh-mount.sh &>/dev/null &&
	    echo "ssh-mount.sh removed from /etc/profile.d" ) ||
	( echo "could not remove ssh-mount.sh from /etc/profile.d" &&
		EXIT_CODE=1 )

    # remove the ssh-mount group
    ( groupdel ssh-mount &>/dev/null && echo "ssh-mount group removed" ) ||
	( echo "ssh-mount group could not be removed" && EXIT_CODE=1 )
    
    # remove the autocomplete rules
    ( rm /etc/bash_completion.d/ssh-mount &>/dev/null &&
	    echo "ssh-mount autocomplete rules removed" ) ||
	( echo "could not remove ssh-mount autocomplete rules" && EXIT_CODE=1 )

    # check for and remove local environmental changes
    if grep --quiet "export PATH=\"\$PATH:$INSTALL_DIR/bin\"" $HOME/.bashrc;
    then
	_sed="/export PATH=\\\"\$PATH:${INSTALL_DIR/"/"/"\\/"}\/bin\\\"/d"
	sed -i "$_sed" $HOME/.bashrc &&
	    echo "PATH adjustment removed from $HOME/.bashrc"
    fi

    # check for and remove keepalive in ssh config
    if [ `grep ServerAliveInterval /etc/ssh/ssh_config | awk '{print $1}'` ] ;
    then
        sed -i "/ServerAliveInterval 15/d" /etc/ssh/ssh_config
    fi
    if [ `grep ServerAliveCountMax /etc/ssh/ssh_config | awk '{print $1}'` ] ;
    then
	sed -i "/ServerAliveCountMax 3/d" /etc/ssh/ssh_config
    fi

    exit $EXIT_CODE
fi
# end uninstall section ################################################

# install sshfs if necessary
_INSTALLED=`dpkg -s sshfs 2>&1 | grep Status | awk '{print $3}'`
if [[ ! $_INSTALLED == "ok" ]] ; then
    apt-get install sshfs
fi

# set the global SSH_MOUNT_DIR env variable and put the install dir on the path
mkdir -p /etc/profile.d &>/dev/null
echo "export SSH_MOUNT_DIR=$SSH_MOUNT_DIR" > /etc/profile.d/ssh-mount.sh
echo "export PATH=$PATH:$INSTALL_DIR/bin" >> /etc/profile.d/ssh-mount.sh

# create the ssh-mount group
SSHFS_GROUP_EXISTS=false
for g in $(cat /etc/group | awk -F ":" '{print $1}') ; do
    [[ $g == "ssh-mount" ]] && SSHFS_GROUP_EXISTS=true
done
( $SSHFS_GROUP_EXISTS && echo "group 'ssh-mount' already exists" ) ||
    ( groupadd ssh-mount && echo "created group 'ssh-mount'" )

# add the user to the ssh-mount group
SSHFS_GROUP_EXISTS=false
for g in $(groups $SUDO_USER) ; do
    [[ $g == "ssh-mount" ]] && SSHFS_GROUP_EXISTS=true
done
( $SSHFS_GROUP_EXISTS && echo "$SUDO_USER already a member of 'ssh-mount'" ) ||
    ( usermod -aG ssh-mount $SUDO_USER &&
	    echo "$SUDO_USER added to group 'ssh-mount'" )


# create ssh-mount folder
if [ -f "$SSH_MOUNT_DIR" ]; then
    echo "Cannot create $SSH_MOUNT_DIR/ directory!"
    exit 2
fi
if [ ! -d "$SSH_MOUNT_DIR" ]; then
    if ( mkdir $SSH_MOUNT_DIR ) ; then
	echo "$SSH_MOUNT_DIR created"
    else
	echo "Could not create $SSH_MOUNT_DIR"
	exit 2
    fi
    if ! ( chmod 774 $SSH_MOUNT_DIR && chgrp ssh-mount $SSH_MOUNT_DIR ) ; then
	echo "Could not set group or permissions on $SSH_MOUNT_DIR"
	EXIT_CODE=1
    fi
fi

# Install the ssh-mount script
SOURCE_DIR="$( dirname "$BASH_SOURCE[0]}" )/bin"
mkdir -p $INSTALL_DIR/bin &>/dev/null
chgrp -R ssh-mount $INSTALL_DIR &>/dev/null
cp $SOURCE_DIR/ssh-mount.sh $INSTALL_DIR/bin/ssh-mount
chmod -R 755 $INSTALL_DIR/bin/ssh-mount
echo "ssh-mount installed in $INSTALL_DIR/bin/"

# add auto complete info
cp $SOURCE_DIR/_ssh-mount.sh /etc/bash_completion.d/ssh-mount
chown root /etc/bash_completion.d/ssh-mount
chgrp root /etc/bash_completion.d/ssh-mount
chmod 644 /etc/bash_completion.d/ssh-mount
echo "Autocomplete rules added"

# add the $INSTALL_DIR/bin directory to path
# uncomment if /etc/profile.d/sshd.sh is not exporting the correct path
#if ! grep --quiet "export PATH=\"\$PATH:$INSTALL_DIR/bin\"" $HOME/.bashrc; then
#    printf "\nexport PATH=\"\$PATH:$INSTALL_DIR/bin\"\n" >> $HOME/.bashrc
#fi

# add a keepalive interval to ssh_config
if [ ! `grep ServerAliveInterval /etc/ssh/ssh_config | awk '{print $1}'` ]; then
    echo "    ServerAliveInterval 15" >> /etc/ssh/ssh_config
fi
if [ ! `grep ServerAliveCountMax /etc/ssh/ssh_config | awk '{print $1}'` ]; then
    echo "    ServerAliveCountMax 3" >> /etc/ssh/ssh_config
fi
echo "Keep alive rules added to /etc/ssh/ssh_config"

echo "You must log out before all changes will take effect"

exit $EXIT_CODE


